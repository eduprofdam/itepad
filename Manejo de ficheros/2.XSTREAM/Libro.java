
package xstreamsxml;


public class Libro {
    String nombre;
    String genero ;
    String isbn ;
    String editorial;
    
   public Libro (String nombre, String apellido,String isbn,String editorial){
       
       this.nombre = nombre;
       this.genero = apellido;
       this.isbn = isbn;
       this.editorial = editorial;
       
               
       
   }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String edad) {
        this.editorial = edad;
    }
    
}
