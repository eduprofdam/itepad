/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xstreamsxml;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        Libro l1 = new Libro("libro1", "drama", "IUDH-65", "Anaya");

        XStream xstream = new XStream(new DomDriver());
        xstream.alias("libro", Libro.class);
        String salida = xstream.toXML(l1);
        System.out.println(salida);
        

        File f1 = new File("archivoXML");
        if (!f1.exists()) {
            f1.createNewFile();
        }
        

        FileWriter fw = new FileWriter(f1);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(salida);
        bw.flush();
        bw.close ();
        System.out.println("Escritura realizada con éxito");
    }
}
