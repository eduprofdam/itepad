import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class Entrada {

	public static void main(String[] args) {
		File directorio = new File("ejercicios");

		if (!directorio.mkdir()) {
			System.out.println("Error al crear directorio");
		} else {
			System.out.println("Directorio creado");
		}

		File fichero = new File("ejercicios/ejercicio1.txt");
		File fichero2 = new File("ejercicios/ejercicio2.txt");
		// File fichero = new File(directorio,"ejercicio1.txt");
		// File fichero = new File(directorio,"ejercicio2.txt");
		try {
			fichero.createNewFile();
			System.out.println("Fichero creado" + fichero.getName());

			fichero2.createNewFile();
			System.out.println("Fichero creado" + fichero.getName());
			// Tamaño del fichero
			System.out.println("El tamaño del fichero es: " + fichero.length());

		} catch (IOException ex) { // Excepción generar de cualquier operacion de entrada y salida de java
			System.err.println("No se ha podido crear el fichero");
			ex.getMessage();
		}
		File[] archivos;
		archivos = directorio.listFiles();
		for (File item : archivos) {
			System.out.println(item.getName());

		}
		// Borrar fichero
		if (fichero.delete()) {
			System.out.println("Fichero borrado correctamente");
		} else {
			System.err.println("No se ha podido borrar el fichero");
		}
		// Borrar fichero de nuevo
		if (fichero.delete()) {
			System.out.println("Fichero borrado correctamente");
		} else {
			System.err.println("No se ha podido borrar el fichero");
		}

	}

}