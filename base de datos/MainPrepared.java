package conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MainPrepared {

	public static void main(String[] args) {
		Conexion con = new Conexion("jdbc:mysql://localhost/dam", "root", "admin");

		con.establecerConexion();
		
		// Para llamar al metodo prepareStatement necesitamos una connection.
		Connection connection = con.getConnection();
		PreparedStatement stmt;
		  try {
	            //stmt = con.getConnection().prepareStatement("select * from alumnos where id = ?");
	            //stmt = connection.prepareStatement("select * from alumnos where id = ?");
			  
	            stmt = connection.prepareStatement("select * from alumnos where nombre like ? and edad=? limit ?");
	            stmt.setString(1, "eduardo");
	            stmt.setInt(2, 33);
	            stmt.setInt(3, 1);
	            
	            ResultSet rs = stmt.executeQuery();

	            while (rs.next()) {
	                
	                int id = rs.getInt(1);
	               System.out.println(id);
	               String nombre = rs.getString(2);
	               System.out.println(nombre);
	            }

	        } catch (SQLException e) {
	            
	            e.printStackTrace();
	            
	        }

	}

}
